# Building Web Applications with Clojure examples

Sample code for [Building Web Applications with Clojure video course](https://www.packtpub.com/web-development/building-web-applications-clojure-video) 
from Packt Publishing.

This video course is bit old and some examples were outdated and were using 
deprecated or even removed code. So I had to do some research to figure out 
how to make outdated code to work with current version of used libraries.

Still this is a good introduction to `Ring` library and to how to build simple
web application with `Clojure`.

*Copyright © 2017 Eduard Luhtonen*