(defproject project1 "0.1.0-SNAPSHOT"
  :description "FIXME: write description"
  :url "http://example.com/FIXME"
  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/clojurescript "1.9.293"]
                 [org.postgresql/postgresql "9.4.1212"]
                 ;[org.clojure/java.jdbc "0.7.0-alpha1"]
                 [korma "0.4.3"]
                 [com.novemberain/monger "3.1.0"]
                 [cheshire "5.6.3"]
                 [ring "1.5.0"]
                 [compojure "1.5.2"]
                 [hiccup "1.0.5"]]
  :plugins [[lein-ring "0.10.0"]
            [lein-cljsbuild "1.1.5"]]
  :ring {;;:handler project1.core/example-handler
         ;;:handler project1.core/route-handler
         ;;:handler project1.core/wrapping-handler
         :handler project1.core/full-handler
         :init    project1.core/on-init
         :port    4001
         :destroy project1.core/on-destroy}
  :cljsbuild {:builds [{:source-paths ["src-cljs"]
                        :compiler {:output-to "resources/public/app.js"
                                   :optimizations :whitespace
                                   :pretty-print true}}]})
