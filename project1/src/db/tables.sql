-- create user
-- createuser -P clojure_blog
-- create database
-- createdb -Oclojure_blog -Eutf8 clojure_blog

-- create table
CREATE TABLE entries (
  id serial NOT NULL,
  title TEXT,
  body TEXT,
  CONSTRAINT pk PRIMARY KEY (id)
);
