(ns project1.blog
  (:require
    [clojure.data.json :as json]
    [clojure.walk :as walk]
    ; relation database
    ;[project1.db :as db]
    ; jdbc + sql storage
    ;[clojure.java.jdbc :as jdbc]
    ; korma ORM
    ;[korma.core :as korma]
    ; NoSQL
    [monger.core :as mg]
    [monger.collection :as mc]
    [monger.json]
    [cheshire.core]
    [project1.route :as route])
  (:import org.bson.types.ObjectId))

; in memory storage
;(defonce BLOG (atom {}))

; in memory storage
;(defonce ID (atom 0))

; NoSQL
(def db
  (->
    (mg/connect)
    (mg/get-db "clojure_blog")))

(defn get-blog-entries []
  ; in memory storage
  ;(sort-by :id (vals @BLOG))
  ; jdbc + sql storage
  ;(jdbc/query db/postgresql-db
  ;            ["SELECT id, title, body FROM entries"])
  ; korma ORM
  ;(korma/select db/entries)
  ; NoSQL
  (mc/find-maps db "entries"))

(defn add-blog-entry [entry]
  ; in memory storage
  ;(println entry)
  ;(let [id (swap! ID inc)]
  ;  (get (swap! BLOG assoc id (assoc entry :id id)) id))
  ; jdbc + sql storage
  ;(jdbc/with-db-transaction [database db/postgresql-db]
  ;                     (jdbc/insert! database :entries (select-keys entry [:title :body])))
  ; korma ORM
  ;(korma/insert db/entries (korma/values (select-keys entry [:title :body])))
  ; NoSQL
  (let [entry (assoc entry :_id (ObjectId.))]
    (mc/insert db "entries" entry)
    entry))

(defn get-blog-entry [id]
  ; in memory storage
  ;(get @BLOG id)
  ; jdbc + sql storage
  ;(first (jdbc/query db/postgresql-db
  ;                   ["SELECT id, title, body FROM entries where id=?" id]))
  ; korma ORM
  ;(first (korma/select db/entries
  ;                     (korma/where {:id id})))
  ; NoSQL
  (mc/find-one-as-map db "entries" {:_id (ObjectId. id)}))

(defn update-blog-entry [id entry]
  ; in memory storage
  ;(when (get-blog-entry id)
  ;  (get (swap! BLOG assoc id entry) id))
  ; jdbc + sql storage
  ;(jdbc/with-db-transaction [database db/postgresql-db]
  ;                          (jdbc/update! database :entries
  ;                                        (select-keys entry [:title :body])
  ;                                        ["id=?" id]))
  ; korma ORM
  ;(korma/update db/entries
  ;              (korma/set-fields (select-keys [:title :body]))
  ;              (korma/where {:id id}))
  ;(get-blog-entry id)
  ; NoSQL
  (let [old-entry (get-blog-entry id)
        new-entry (merge old-entry entry)]
    (mc/update-by-id db "entries" (:_id old-entry) new-entry)
    new-entry))

(defn alter-blog-entry [id entry-values]
  ; in memory storage
  ;(when (get-blog-entry id)
  ;  (get (swap! BLOG update-in [id] merge entry-values) id))
  ; jdbc + sql storage
  (update-blog-entry id entry-values))

(defn delete-blog-entry [id]
  ;(when (get-blog-entry id)
  (when-let [entry (get-blog-entry id)]
    ; in memory storage
    ;(swap! BLOG dissoc id)
    ; jdbc + sql storage
    ;(jdbc/with-db-transaction [database db/postgresql-db]
    ;                          (jdbc/delete! database :entries ["id=?" id]))
    ; korma ORM
    ;(korma/delete db/entries (korma/where {:id id}))
    ; NoSQL
    (mc/remove-by-id db "entries" (:_id entry))
    {:id id}))

(defn json-response [data]
  (when data
    {;:body (json/write-str data)
     :body (cheshire.core/generate-string data)
     :headers {"Content-type" "application/json"}}))

(defn json-body [request]
  (walk/keywordize-keys
    (json/read-str (slurp (:body request)))))

(defn json-error-handler [handler]
  (fn [request]
    (try
      (handler request)
      (catch Throwable throwable
        (assoc (json-response {:message (.getLocalizedMessage throwable)
                               :stacktrace (map str (.getStackTrace throwable))})
          :status 500)))))

(defn get-id [request]
  ;(Long/parseLong (-> request :route-params :id))
  (-> request :route-params :id))

(defn get-handler [request]
  (json-response (get-blog-entries)))

(defn post-handler [request]
  (json-response (add-blog-entry (json-body request))))

(defn get-entry-handler [request]
  (json-response (get-blog-entry (get-id request))))

(defn put-handler [request]
  (json-response (update-blog-entry (get-id request) (json-body request))))

(defn delete-handler [request]
  (json-response (delete-blog-entry (get-id request))))

(def blog-handler
  (->
    (route/routing
      (route/with-route-matches :get "/" get-handler)
      (route/with-route-matches :post "/" post-handler)
      (route/with-route-matches :get "/:id" get-entry-handler)
      (route/with-route-matches :put "/:id" put-handler)
      (route/with-route-matches :delete "/:id" delete-handler))
    json-error-handler))
